package us.bowmansarrow.moneyproject;

import java.util.Arrays;
import java.util.Collection;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class MoneyComparisonTest {

    @Parameters(name = "{index}: {0}, {1}")
    public static Collection<Money[]> data() {
        return Arrays.asList(new Money[][]{
            {new Money(1), new Money(0, 01)},
            {new Money(1, 99), new Money(1, 98)},
            {new Money(1, 100), new Money(1, 99)}
        });
    }

    public MoneyComparisonTest(Money in1, Money in2) {
        this.input1 = in1;
        this.input2 = in2;
    }

    @Test
    public void greaterThanTest() {
        assertThat(input1.greaterThan(input2), is(true));
        assertThat(input2.greaterThan(input1), is(false));
    }

    @Test
    public void lessThanTest() {
        assertThat(input2.lessThan(input1), is(true));
        assertThat(input2.greaterThan(input1), is(false));
    }

    private final Money input1;
    private final Money input2;

}
