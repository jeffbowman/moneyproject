package us.bowmansarrow.moneyproject;

import com.pholser.junit.quickcheck.ForAll;
import com.pholser.junit.quickcheck.generator.InRange;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.number.OrderingComparison.greaterThan;
import static org.junit.Assert.assertThat;
import static org.junit.Assume.assumeThat;
import org.junit.Test;
import org.junit.contrib.theories.Theories;
import org.junit.contrib.theories.Theory;
import org.junit.contrib.theories.suppliers.TestedOn;
import org.junit.runner.RunWith;

@RunWith(Theories.class)
public class MoneyTest {
    /*
     * Start with 'normal' junit tests
     */

    @Test
    public void moneyIsInstantiatedCorectly() {
        final Money fiveDollars = new Money(5);
        final Money oneDollarAnd25Cents = new Money(1, 25);
        final Money twoDollarsAnd10Cents = new Money("2.10");
        final Money threeDollarsAnd1Cent = new Money(3, 1);
        final Money fourDollars = new Money(4, 0);
        final Money sixDollars = new Money(5, 100);

        assertThat(fiveDollars.toString(), is("$5.00"));
        assertThat(oneDollarAnd25Cents.toString(), is("$1.25"));
        assertThat(twoDollarsAnd10Cents.toString(), is("$2.10"));
        assertThat(threeDollarsAnd1Cent.toString(), is("$3.01"));
        assertThat(fourDollars.toString(), is("$4.00"));
        assertThat(sixDollars.toString(), is("$6.00"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void moneyDollarConstructorThrowsExceptionWithNegativeNumbers() {
        new Money(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void moneyWithPositiveDollarAndNegativeCentsThrowsException() {
        new Money(1, -1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void moneyStringConstructorThrowsExceptionsWithNonNumericValues() {
        new Money("a.b");
    }

    @Test(expected = IllegalArgumentException.class)
    public void moneyStringConstructorThrowsExceptionsWithNoDecimalPoint() {
        new Money("1");
    }

    /*
     * Replace happy path Test with some Theory tests
     */
    @Theory
    public void canConstructWithAnyPositiveDollarValue(@ForAll(sampleSize = 10000) int dollarValue) {
        assumeThat(dollarValue, greaterThan(0));
        final Money money = new Money(dollarValue);
        assertThat(money.toString(), is(String.format("$%d.00", dollarValue)));
    }

    @Theory
    public void canConstructCorrectly(@ForAll @InRange(minInt = -10, maxInt = 100) int dollars,
            @ForAll @InRange(minInt = 1, maxInt = 99) int cents) {
        assumeThat(dollars, greaterThan(0)); // InRange for dollars allows negative numbers, this should weed them out
        final Money money = new Money(dollars, cents);
        assertThat(money.toString(), is(String.format(MONEY_FORMAT, dollars, cents)));
    }

    @Theory
    public void centsGreaterThan99ConvertToDollars(@ForAll int dollars,
            @ForAll @InRange(minInt = 100, maxInt = 500) int cents) {
        assumeThat(dollars, greaterThan(0));
        final Money money = new Money(dollars, cents);
        final int increment = cents / 100;
        final int centsLeft = cents - increment * 100;
        assumeThat(centsLeft, greaterThan(0));
        assertThat(money.toString(), is(String.format(MONEY_FORMAT, dollars + increment, centsLeft)));
    }

    @Theory
    public void centsGreaterThan99ConvertToDollarsOnStringConstructor(@ForAll int dollars,
            @ForAll @InRange(minInt = 100, maxInt = 500) int cents) {
        assumeThat(dollars, greaterThan(0));
        final Money money = new Money(String.format("%d.%02d", dollars, cents));
        final int increment = cents / 100;
        final int centsLeft = cents - increment * 100;
        final int expectedDollars = dollars + increment;
        assertThat(money.toString(), is(String.format(MONEY_FORMAT, expectedDollars, centsLeft)));
    }

    @Theory
    public void addReturnsTheSum(@ForAll @InRange(minInt = 10, maxInt = 200) int dollars,
            @ForAll @InRange(minInt = 1, maxInt = 99) int cents) {
        int[] addedValues = convertCents(cents + 1);
        int otherDollars = dollars + 1 + addedValues[0];
        int otherCents = addedValues[1];
        int[] expectedAddedValues = convertCents(cents + otherCents);
        int expectedDollars = dollars + otherDollars + expectedAddedValues[0];
        int expectedCents = expectedAddedValues[1];
        String expectedString = String.format(MONEY_FORMAT, expectedDollars, expectedCents);

        final Money money1 = new Money(dollars, cents);
        final Money money2 = new Money(otherDollars, otherCents);
        final Money result = money1.add(money2);

        assertThat(result.toString(), is(expectedString));
    }

    @Theory
    public void subReturnsTheDifference(@ForAll @InRange(minInt = 10, maxInt = 500) int dollars,
            @ForAll @InRange(minInt = 1, maxInt = 99) int cents) {
        int otherDollars = dollars / 2;
        int otherCents = cents / 2;
        int expectedDollars = dollars - otherDollars;
        int expectedCents = cents - otherCents;
        final String expected = String.format(MONEY_FORMAT, expectedDollars, expectedCents);
        final Money money1 = new Money(dollars, cents);
        final Money money2 = new Money(otherDollars, otherCents);
        final Money result1 = money1.sub(money2);
        final Money result2 = money2.sub(money1);
        assertThat(result1.toString(), is(expected));
        assertThat(result2.toString(), is(result1.toString()));
    }

    @Theory
    public void equalsShouldPassWhenTheAmountsAreTheSame(@ForAll(sampleSize = 10) @InRange(minInt = 10, maxInt = 100) int dollars,
            @ForAll(sampleSize = 10) @InRange(minInt = 1, maxInt = 99) int cents) {
        final Money m1 = new Money(dollars, cents);
        final Money m2 = new Money(dollars, cents);

        assertThat(m1.equals(m1), is(true));
        assertThat(m1.equals(m2), is(true));
    }

    @Theory
    public void hashCodeTest(@TestedOn(ints = {1, 2, 3, 5, 8, 13, 21}) int dollars,
            @TestedOn(ints = {99, 80, 50, 25, 12, 10, 1}) int cents) {
        int hash = 7;
        hash = 83 * hash + dollars;
        hash = 83 * hash + cents;
        assertThat(new Money(dollars, cents).hashCode(), is(hash));
    }

    private int[] convertCents(final int cents) {
        int dollarIncrement = 0;
        int centsLeft = cents;
        if (cents > 99) {
            dollarIncrement = cents / 100;
            centsLeft = cents - dollarIncrement * 100;
        }
        return new int[]{dollarIncrement, centsLeft};
    }

    private static final String MONEY_FORMAT = "$%d.%02d";
}
