package us.bowmansarrow.moneyproject;

public class Money {

    public Money(final int dollars) {
        this(dollars, 0);
    }

    public Money(final String moneyValue) {
        if (!moneyValue.contains(".")) {
            throw new IllegalArgumentException("Invalid money representation");
        }
        final String[] values = moneyValue.split("\\.");
        final int[] convertedValues = Money.convertCents(Integer.valueOf(values[1]));
        dollars = Integer.valueOf(values[0]) + convertedValues[0];
        cents = convertedValues[1];
    }

    public Money(final int dollars, final int cents) {
        if (dollars < 0 || cents < 0) {
            throw new IllegalArgumentException(String.format("Money cannot be created with negative numbers d: %d c: %d", dollars, cents));
        }
        final int[] convertedValues = Money.convertCents(cents);
        this.dollars = dollars + convertedValues[0];
        this.cents = convertedValues[1];
    }

    public Money add(final Money valueToAdd) {
        return new Money(this.dollars + valueToAdd.dollars, this.cents + valueToAdd.cents);
    }

    public Money sub(final Money other) {
        int lessDollars = Math.abs(dollars - other.dollars);
        int lessCents = Math.abs(cents - other.cents);
        return new Money(lessDollars, lessCents);
    }

    public boolean greaterThan(final Money other) {
        return dollars * 100 + cents > other.dollars * 100 + other.cents;
    }

    public boolean lessThan(final Money other) {
        return dollars * 100 + cents < other.dollars * 100 + other.cents;
    }

    @Override
    public boolean equals(final Object other) {
        if (other instanceof Money) {
            final Money o = (Money) other;
            return o == this ? true : dollars * 100 + cents == o.dollars * 100 + o.cents;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + this.dollars;
        hash = 83 * hash + this.cents;
        return hash;
    }

    @Override
    public String toString() {
        if (str == null) str = String.format("$%d.%02d", dollars, cents);
        return str;
    }

    private static int[] convertCents(final int cents) {
        int dollarIncrement = 0;
        int centsLeft = cents;
        if (cents > 99) {
            dollarIncrement = cents / 100;
            centsLeft = cents - dollarIncrement * 100;
        }
        return new int[]{dollarIncrement, centsLeft};
    }

    private final int dollars;
    private final int cents;
    private String str = null;
}
